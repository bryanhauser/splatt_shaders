// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomColorActor.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine.h"
#include "Kismet/KismetMathLibrary.h"


// Sets default values
ARandomColorActor::ARandomColorActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Prepares mesh and makes it the root
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	//Finds custom material created in content
	static ConstructorHelpers::FObjectFinder<UMaterial>MAT(TEXT("Material'/Game/Shader/TeamColors/MAT_TeamNONEColor.MAT_TeamNONEColor'"));
	
	//If material was found, assigns it to a variable
	if (MAT.Object != NULL) {
		m_Dynamic = (UMaterial*)MAT.Object;
	}
}

// Called when the game starts or when spawned
void ARandomColorActor::BeginPlay()
{
	Super::BeginPlay();

	//If variable exists, creates a material instance
	if (m_Dynamic) {
		MaterialInstance = UMaterialInstanceDynamic::Create(m_Dynamic, this);
	}

	//If material instance exists then sets it to this object
	if (MaterialInstance) {
		MeshComponent->SetMaterial(0, MaterialInstance);
	}
	
}

// Called every frame
void ARandomColorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Changes color to a team one color
void ARandomColorActor::ChangeRed()
{
	MaterialInstance->SetScalarParameterValue("NoTeam", 1.f);
	MaterialInstance->SetScalarParameterValue("TeamColor", 0.f);
}

void ARandomColorActor::ChangeBlue()
{
	MaterialInstance->SetScalarParameterValue("NoTeam", 1.f);
	MaterialInstance->SetScalarParameterValue("TeamColor", 1.f);
}

void ARandomColorActor::ChangeGrey()
{
	MaterialInstance->SetScalarParameterValue("NoTeam", 0.f);
}

