// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Splatt_HauserCharacter.h"
#include "Splatt_HauserProjectile.h"
#include "RandomColorActor.h"
#include "Math/Color.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameplayTagsModule.h"
#include "GameplayTagsSettings.h"
#include "GameplayTags.h"
#include "GameplayTagsManager.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ASplatt_HauserCharacter

ASplatt_HauserCharacter::ASplatt_HauserCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	//FP_Gun->SetupAttachment(RootComponent);
	

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	//Initializes base values for variables
	baseWeaponDamage = 1.f;
	totalHealth = 10.f;
	totalAmmo = 15;
	currentHealth = totalHealth;
	currentAmmo = totalAmmo;

	
}

void ASplatt_HauserCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	//Sets initial value for team for testing. TODO: add functionality in game to select team at the start.
	FGameplayTag TeamColor = FGameplayTag::RequestGameplayTag(FName("Team.Blue"));

}


//////////////////////////////////////////////////////////////////////////
// Input

void ASplatt_HauserCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ASplatt_HauserCharacter::OnFire);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ASplatt_HauserCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ASplatt_HauserCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASplatt_HauserCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASplatt_HauserCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASplatt_HauserCharacter::LookUpAtRate);

	//Adds functionality to change team at runtimef or testing. TODO: add functionality to select team at game start only.
	PlayerInputComponent->BindAction("ChangeTeam", IE_Pressed, this, &ASplatt_HauserCharacter::ChangeTeam);
	//Adds functionality to reload ammo at any time. TODO: will need to stop it from going into negatives and will want to add a time delay to make it costly to reload during battle
	PlayerInputComponent->BindAction("ReloadAmmo", IE_Pressed, this, &ASplatt_HauserCharacter::ReloadAmmo);
}

void ASplatt_HauserCharacter::ChangeTeam()
{
	//If team is already blue, will switch to red. If already red, will switch to none. And if no team, will switch to blue when pressed.
	if (TeamColor.GetTagName() == (FName("Team.Blue")))
	{
		TeamColor = FGameplayTag::RequestGameplayTag(FName("Team.Red"));
	}
	else if (TeamColor.GetTagName() == (FName("Team.Red")))
	{
		TeamColor = FGameplayTag::RequestGameplayTag(FName("Team.None"));
	}
	else
	{
		TeamColor = FGameplayTag::RequestGameplayTag(FName("Team.Blue"));
	}
}

FGameplayTag ASplatt_HauserCharacter::getGameplayTag()
{
	return TeamColor;
}

//A getter for team color to be used in UI settings and to change the color of objects as necessary.
FLinearColor ASplatt_HauserCharacter::getTeamColor()
{
	if (TeamColor.GetTagName() == (FName("Team.Blue")))
	{
		return FLinearColor(0.f, 0.f, 1.f);
	}
	else if (TeamColor.GetTagName() == (FName("Team.Red")))
	{
		return FLinearColor(1.f, 0.f, 0.f);
	}
	else
	{
		return FLinearColor(0.16f, 0.16f, 0.16f);
	}
}

//Same as above, but returns an FString that can be used to call other functions.
FString ASplatt_HauserCharacter::getTeamColorName()
{
	if (TeamColor.GetTagName() == (FName("Team.Blue")))
	{
		return FString("Blue");
	}
	if (TeamColor.GetTagName() == (FName("Team.Red")))
	{
		return FString("Red");
	}
	else
	{
		return FString("None");
	}
}

void ASplatt_HauserCharacter::OnFire()
{
	//Adds in functionality to only fire if 1 or more pieces of ammo
	if (currentAmmo >= 1)
	{
		//Currently using vectors instead of projectiles for hitting an object. TODO: set up projectile to hit and add splat effects on objects.
		FHitResult HitResult;
		FVector StartLocation = FirstPersonCameraComponent->GetComponentLocation();
		FRotator Direction = FirstPersonCameraComponent->GetComponentRotation();
		FVector EndLocation = StartLocation + Direction.Vector() * 10000;
		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);


		if (GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, QueryParams))
		{
			TArray<UStaticMeshComponent*>StaticMeshComponents = TArray<UStaticMeshComponent*>();
			AActor* HitActor = HitResult.GetActor();
			if (NULL != HitActor)
			{
				HitActor->GetComponents<UStaticMeshComponent>(StaticMeshComponents);
				if (ARandomColorActor* CubeObject = Cast<ARandomColorActor>(HitActor))
				{
					//Changes color of the cubes when hit based on what team the player is on.
					if (TeamColor.GetTagName() == (FName("Team.Blue")))
					{
						CubeObject->ChangeBlue();
					}
					else if (TeamColor.GetTagName() == (FName("Team.Red")))
					{
						CubeObject->ChangeRed();
					}
					else
					{
						CubeObject->ChangeGrey();
					}

				}

			}
		}

		// try and play the sound if specified
		if (FireSound != NULL)
		{
			//UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}

		// try and play a firing animation if specified
		if (FireAnimation != NULL)
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
			if (AnimInstance != NULL)
			{
				AnimInstance->Montage_Play(FireAnimation, 1.f);
			}
		}
		currentAmmo -= 1;
	}
	
	
}

void ASplatt_HauserCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ASplatt_HauserCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void ASplatt_HauserCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void ASplatt_HauserCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void ASplatt_HauserCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ASplatt_HauserCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ASplatt_HauserCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASplatt_HauserCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool ASplatt_HauserCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ASplatt_HauserCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &ASplatt_HauserCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ASplatt_HauserCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}

void ASplatt_HauserCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float ASplatt_HauserCharacter::DealDamage()
{
	//Can add in different multipliers to make a more variable damage profile.
	float totalWeaponDamage;
	totalWeaponDamage = baseWeaponDamage * UKismetMathLibrary::RandomFloatInRange(.8f, 1.2f);
	return totalWeaponDamage;
}

void ASplatt_HauserCharacter::ReloadAmmo()
{
	currentAmmo = totalAmmo;
}

//Getter and setter functions for player variables
float ASplatt_HauserCharacter::getTotalHealth()
{
	return totalHealth;
}

void ASplatt_HauserCharacter::setTotalHealth(float health)
{
	totalHealth = health;
}

float ASplatt_HauserCharacter::getCurrentHealth()
{
	return currentHealth;
}

void ASplatt_HauserCharacter::setCurrentHealth(float health)
{
	currentHealth += health;
}

int ASplatt_HauserCharacter::getTotalAmmo()
{
	return totalAmmo;
}

void ASplatt_HauserCharacter::setTotalAmmo(int ammo)
{
	totalAmmo = ammo;
}

int ASplatt_HauserCharacter::getCurrentAmmo()
{
	return currentAmmo;
}

void ASplatt_HauserCharacter::setCurrentAmmo(int ammo)
{
	currentAmmo += ammo;
}
