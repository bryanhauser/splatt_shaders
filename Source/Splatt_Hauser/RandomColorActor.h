// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RandomColorActor.generated.h"

UCLASS()
class SPLATT_HAUSER_API ARandomColorActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARandomColorActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "ColorMesh")
		class UStaticMeshComponent* MeshComponent;

	UPROPERTY()
		UMaterialInstanceDynamic* MaterialInstance;

	UPROPERTY()
		UMaterialInterface* m_Dynamic;

	UFUNCTION(BlueprintCallable)
		void ChangeRed();

	UFUNCTION(BlueprintCallable)
		void ChangeBlue();
	
	UFUNCTION(BlueprintCallable)
		void ChangeGrey();
};
