// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Splatt_HauserCharacter.h"
#include "GameFramework/Actor.h"
#include "CentralPillar.generated.h"

UCLASS()
class SPLATT_HAUSER_API ACentralPillar : public AActor
{
	GENERATED_BODY()


	
	
public:	
	// Sets default values for this actor's properties
	ACentralPillar();

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class USphereComponent* SphereComponent;

	UFUNCTION()
	void OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	float PointProgression;
	bool isControlPoint;
	bool isContestedPoint;
	TArray<AActor*> TriggerPlayers;
	TArray<FString> TeamNames;

	class ASplatt_HauserCharacter* player;

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	float getPointProgression();
	void setPointProgression(float DeltaTime);

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	FString InControlTeam();
	UFUNCTION(BlueprintCallable, Category = Gameplay)
	bool IsPointBeingContested();

	UPROPERTY(EditAnywhere, Category = "ColorMesh")
	class UStaticMeshComponent* BaseShape;

	UPROPERTY()
	UMaterialInstanceDynamic* MaterialInstance;

	UPROPERTY()
	UMaterialInterface* m_Dynamic;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
