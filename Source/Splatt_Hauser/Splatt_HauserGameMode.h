// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Splatt_HauserGameMode.generated.h"

UCLASS(minimalapi)
class ASplatt_HauserGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASplatt_HauserGameMode();
};



