// Fill out your copyright notice in the Description page of Project Settings.

#include "CentralPillar.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/SphereComponent.h"


// Sets default values
ACentralPillar::ACentralPillar()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Creates the collision boundary and the base shape for central object
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	BaseShape = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShapeComponent"));

	//Finds material in content folder
	static ConstructorHelpers::FObjectFinder<UMaterial>MAT(TEXT("Material'/Game/CentralPillar/MAT_CentralPillar.MAT_CentralPillar'"));
	

	//If material was found, assigns it to a variable
	if (MAT.Object != NULL) 
	{
		m_Dynamic = (UMaterial*)MAT.Object;
	}

	//Sets up dynamic overlap functionality
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ACentralPillar::OnSphereBeginOverlap);
	SphereComponent->OnComponentEndOverlap.AddDynamic(this, &ACentralPillar::OnSphereEndOverlap);

	//Initializes some base variables
	PointProgression = 0.f;
	isControlPoint = false;
	isContestedPoint = false;
	TeamNames.Init("None", 3);

}

void ACentralPillar::OnSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap Begin")); //Debugging Purposes
		TriggerPlayers.AddUnique(OtherActor);

		//Pulls in the gameplay tag with the team name from the player script when player enters sphere
		if (OtherActor == player && player != NULL)
		{
			TeamNames.Remove("None");
			FString team = player->getTeamColorName();
			TeamNames.AddUnique(team);
		
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, team);//Debugging Purposes
		}
	}
}

void ACentralPillar::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap End")); //Debugging Purposes
		TriggerPlayers.Remove(OtherActor);

		//Removes the gameplay tag with the team name from the array once that player leaves the sphere
		if (OtherActor == player && player != NULL)
		{
			TeamNames.AddUnique("None");
			FString team = player->getTeamColorName();
			TeamNames.Remove(team);

			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, team);//Debugging Purposes
		}
	}
}

float ACentralPillar::getPointProgression()
{
	return PointProgression;
}

void ACentralPillar::setPointProgression(float DeltaTime)
{
	//Conditionals for if both teams are in center will assign 'CONTESTED' as true, if only one is in the center will assign 'CONTROL' to true
	if (TeamNames.Contains("Blue") && TeamNames.Contains("Red"))
	{
		isContestedPoint = true;
	}
	else if (TeamNames.Contains("Blue") || TeamNames.Contains("Red")) 
	{
		isControlPoint = true;
	}
	else {
		isControlPoint = false;
		isContestedPoint = false;
	}

	//Progression will increase if it is 'CONTROL' and less than full, will stop at full if equal to or more than full, will not increase if 'CONTESTED', and will decrease if not 'CONTESTED' and not 'CONTROL'
	if (isControlPoint && PointProgression < 1.f)
	{
		PointProgression += .01 * DeltaTime;
	}
	else if (isControlPoint && PointProgression >= 1.f)
	{
		PointProgression = 1.f;
	}
	else if (isContestedPoint)
	{
		PointProgression = PointProgression;
	}
	else if(!isContestedPoint && !isControlPoint && PointProgression >= 0.f)
	{
		PointProgression -= .005 * DeltaTime;
	}
	else {
		PointProgression = 0.f;
	}
}

FString ACentralPillar::InControlTeam()
{
	FString* TeamPtr = TeamNames.GetData();
	return TeamPtr[0];
}

bool ACentralPillar::IsPointBeingContested()
{
	if (TeamNames.Num() > 1)
	{
		return true;
	}
	else 
	{
		return false;
	}

}



// Called when the game starts or when spawned
void ACentralPillar::BeginPlay()
{
	Super::BeginPlay();

	//If variable exists, creates a material instance
	if (m_Dynamic) {
		MaterialInstance = UMaterialInstanceDynamic::Create(m_Dynamic, this);
	}

	//If material instance exists then sets it to this object
	if (MaterialInstance) {
		BaseShape->SetMaterial(0, MaterialInstance);
	}

	player = Cast<ASplatt_HauserCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	
}

// Called every frame
void ACentralPillar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	setPointProgression(DeltaTime);


}

