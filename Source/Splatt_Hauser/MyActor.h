// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include "MyActor.generated.h"


UCLASS()
class SPLATT_HAUSER_API AMyActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyActor();

	//Default dimensions of the map
	float floorLength = 5000.f;
	float floorWidth = 5000.f;
	float wallWidth = 10.f;
	float wallHeight = 500.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void CreateSquare();
	virtual void GenerateBoxMesh();

	//Sets up each wall as a separate piece. Will research better way to combine all pieces without needing to make each a separate object
	virtual void CreateFirstWall(FVector BoxRadius, TArray <FVector> &Vertices, TArray <int32> &Triangles, TArray <FVector> &Normals, TArray <FVector2D> &UVs, TArray <FProcMeshTangent> &Tangents, TArray <FColor> &Colors);
	virtual void CreateSecondWall(FVector BoxRadius, TArray <FVector> &Vertices, TArray <int32> &Triangles, TArray <FVector> &Normals, TArray <FVector2D> &UVs, TArray <FProcMeshTangent> &Tangents, TArray <FColor> &Colors);
	virtual void CreateThirdWall(FVector BoxRadius, TArray <FVector> &Vertices, TArray <int32> &Triangles, TArray <FVector> &Normals, TArray <FVector2D> &UVs, TArray <FProcMeshTangent> &Tangents, TArray <FColor> &Colors);
	virtual void CreateFourthWall(FVector BoxRadius, TArray <FVector> &Vertices, TArray <int32> &Triangles, TArray <FVector> &Normals, TArray <FVector2D> &UVs, TArray <FProcMeshTangent> &Tangents, TArray <FColor> &Colors);

private:
	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* floor_mesh;

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* first_wall_mesh;

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* second_wall_mesh;

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* third_wall_mesh;

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* fourth_wall_mesh;
	
};
