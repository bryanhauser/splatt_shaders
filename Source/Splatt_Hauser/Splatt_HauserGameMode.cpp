// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Splatt_HauserGameMode.h"
#include "Splatt_HauserHUD.h"
#include "Splatt_HauserCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASplatt_HauserGameMode::ASplatt_HauserGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASplatt_HauserHUD::StaticClass();
}
