// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLinearColor;
#ifdef SPLATT_HAUSER_Splatt_HauserCharacter_generated_h
#error "Splatt_HauserCharacter.generated.h already included, missing '#pragma once' in Splatt_HauserCharacter.h"
#endif
#define SPLATT_HAUSER_Splatt_HauserCharacter_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execsetCurrentAmmo) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ammo); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setCurrentAmmo(Z_Param_ammo); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetCurrentAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->getCurrentAmmo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetTotalAmmo) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ammo); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setTotalAmmo(Z_Param_ammo); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTotalAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->getTotalAmmo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetCurrentHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_health); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setCurrentHealth(Z_Param_health); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetCurrentHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->getCurrentHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetTotalHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_health); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setTotalHealth(Z_Param_health); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTotalHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->getTotalHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTeamColorName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=this->getTeamColorName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTeamColor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLinearColor*)Z_Param__Result=this->getTeamColor(); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execsetCurrentAmmo) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ammo); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setCurrentAmmo(Z_Param_ammo); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetCurrentAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->getCurrentAmmo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetTotalAmmo) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_ammo); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setTotalAmmo(Z_Param_ammo); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTotalAmmo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->getTotalAmmo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetCurrentHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_health); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setCurrentHealth(Z_Param_health); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetCurrentHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->getCurrentHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execsetTotalHealth) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_health); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->setTotalHealth(Z_Param_health); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTotalHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->getTotalHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTeamColorName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=this->getTeamColorName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetTeamColor) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FLinearColor*)Z_Param__Result=this->getTeamColor(); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplatt_HauserCharacter(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserCharacter(); \
public: \
	DECLARE_CLASS(ASplatt_HauserCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ASplatt_HauserCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASplatt_HauserCharacter(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserCharacter(); \
public: \
	DECLARE_CLASS(ASplatt_HauserCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ASplatt_HauserCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplatt_HauserCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplatt_HauserCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatt_HauserCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatt_HauserCharacter(ASplatt_HauserCharacter&&); \
	NO_API ASplatt_HauserCharacter(const ASplatt_HauserCharacter&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatt_HauserCharacter(ASplatt_HauserCharacter&&); \
	NO_API ASplatt_HauserCharacter(const ASplatt_HauserCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatt_HauserCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplatt_HauserCharacter)


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ASplatt_HauserCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ASplatt_HauserCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ASplatt_HauserCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ASplatt_HauserCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ASplatt_HauserCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ASplatt_HauserCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ASplatt_HauserCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ASplatt_HauserCharacter, L_MotionController); }


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_13_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
