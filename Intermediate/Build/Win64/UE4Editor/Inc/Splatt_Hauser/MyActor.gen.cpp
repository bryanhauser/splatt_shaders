// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "MyActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMyActor() {}
// Cross Module References
	SPLATT_HAUSER_API UClass* Z_Construct_UClass_AMyActor_NoRegister();
	SPLATT_HAUSER_API UClass* Z_Construct_UClass_AMyActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Splatt_Hauser();
	PROCEDURALMESHCOMPONENT_API UClass* Z_Construct_UClass_UProceduralMeshComponent_NoRegister();
// End Cross Module References
	void AMyActor::StaticRegisterNativesAMyActor()
	{
	}
	UClass* Z_Construct_UClass_AMyActor_NoRegister()
	{
		return AMyActor::StaticClass();
	}
	UClass* Z_Construct_UClass_AMyActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_Splatt_Hauser,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "MyActor.h" },
				{ "ModuleRelativePath", "MyActor.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fourth_wall_mesh_MetaData[] = {
				{ "Category", "MyActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "MyActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_fourth_wall_mesh = { UE4CodeGen_Private::EPropertyClass::Object, "fourth_wall_mesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a0009, 1, nullptr, STRUCT_OFFSET(AMyActor, fourth_wall_mesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_fourth_wall_mesh_MetaData, ARRAY_COUNT(NewProp_fourth_wall_mesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_third_wall_mesh_MetaData[] = {
				{ "Category", "MyActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "MyActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_third_wall_mesh = { UE4CodeGen_Private::EPropertyClass::Object, "third_wall_mesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a0009, 1, nullptr, STRUCT_OFFSET(AMyActor, third_wall_mesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_third_wall_mesh_MetaData, ARRAY_COUNT(NewProp_third_wall_mesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_second_wall_mesh_MetaData[] = {
				{ "Category", "MyActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "MyActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_second_wall_mesh = { UE4CodeGen_Private::EPropertyClass::Object, "second_wall_mesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a0009, 1, nullptr, STRUCT_OFFSET(AMyActor, second_wall_mesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_second_wall_mesh_MetaData, ARRAY_COUNT(NewProp_second_wall_mesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_first_wall_mesh_MetaData[] = {
				{ "Category", "MyActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "MyActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_first_wall_mesh = { UE4CodeGen_Private::EPropertyClass::Object, "first_wall_mesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a0009, 1, nullptr, STRUCT_OFFSET(AMyActor, first_wall_mesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_first_wall_mesh_MetaData, ARRAY_COUNT(NewProp_first_wall_mesh_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_floor_mesh_MetaData[] = {
				{ "Category", "MyActor" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "MyActor.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_floor_mesh = { UE4CodeGen_Private::EPropertyClass::Object, "floor_mesh", RF_Public|RF_Transient|RF_MarkAsNative, 0x00400000000a0009, 1, nullptr, STRUCT_OFFSET(AMyActor, floor_mesh), Z_Construct_UClass_UProceduralMeshComponent_NoRegister, METADATA_PARAMS(NewProp_floor_mesh_MetaData, ARRAY_COUNT(NewProp_floor_mesh_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_fourth_wall_mesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_third_wall_mesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_second_wall_mesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_first_wall_mesh,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_floor_mesh,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AMyActor>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AMyActor::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMyActor, 922947029);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMyActor(Z_Construct_UClass_AMyActor, &AMyActor::StaticClass, TEXT("/Script/Splatt_Hauser"), TEXT("AMyActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMyActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
