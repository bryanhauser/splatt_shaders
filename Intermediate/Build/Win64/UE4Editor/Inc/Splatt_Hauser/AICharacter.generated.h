// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATT_HAUSER_AICharacter_generated_h
#error "AICharacter.generated.h already included, missing '#pragma once' in AICharacter.h"
#endif
#define SPLATT_HAUSER_AICharacter_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execWander) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->Wander(); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execWander) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->Wander(); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAICharacter(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_AAICharacter(); \
public: \
	DECLARE_CLASS(AAICharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(AAICharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAICharacter(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_AAICharacter(); \
public: \
	DECLARE_CLASS(AAICharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(AAICharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAICharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAICharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAICharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAICharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAICharacter(AAICharacter&&); \
	NO_API AAICharacter(const AAICharacter&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAICharacter(AAICharacter&&); \
	NO_API AAICharacter(const AAICharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAICharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAICharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAICharacter)


#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_PRIVATE_PROPERTY_OFFSET
#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_9_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_AICharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
