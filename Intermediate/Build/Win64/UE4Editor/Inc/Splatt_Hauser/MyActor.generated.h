// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATT_HAUSER_MyActor_generated_h
#error "MyActor.generated.h already included, missing '#pragma once' in MyActor.h"
#endif
#define SPLATT_HAUSER_MyActor_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_RPC_WRAPPERS
#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyActor(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_AMyActor(); \
public: \
	DECLARE_CLASS(AMyActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(AMyActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMyActor(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_AMyActor(); \
public: \
	DECLARE_CLASS(AMyActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(AMyActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActor(AMyActor&&); \
	NO_API AMyActor(const AMyActor&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyActor(AMyActor&&); \
	NO_API AMyActor(const AMyActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyActor)


#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__floor_mesh() { return STRUCT_OFFSET(AMyActor, floor_mesh); } \
	FORCEINLINE static uint32 __PPO__first_wall_mesh() { return STRUCT_OFFSET(AMyActor, first_wall_mesh); } \
	FORCEINLINE static uint32 __PPO__second_wall_mesh() { return STRUCT_OFFSET(AMyActor, second_wall_mesh); } \
	FORCEINLINE static uint32 __PPO__third_wall_mesh() { return STRUCT_OFFSET(AMyActor, third_wall_mesh); } \
	FORCEINLINE static uint32 __PPO__fourth_wall_mesh() { return STRUCT_OFFSET(AMyActor, fourth_wall_mesh); }


#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_11_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_MyActor_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_MyActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
