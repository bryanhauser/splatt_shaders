// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Splatt_HauserHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSplatt_HauserHUD() {}
// Cross Module References
	SPLATT_HAUSER_API UClass* Z_Construct_UClass_ASplatt_HauserHUD_NoRegister();
	SPLATT_HAUSER_API UClass* Z_Construct_UClass_ASplatt_HauserHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_Splatt_Hauser();
// End Cross Module References
	void ASplatt_HauserHUD::StaticRegisterNativesASplatt_HauserHUD()
	{
	}
	UClass* Z_Construct_UClass_ASplatt_HauserHUD_NoRegister()
	{
		return ASplatt_HauserHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_ASplatt_HauserHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_Splatt_Hauser,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "Splatt_HauserHUD.h" },
				{ "ModuleRelativePath", "Splatt_HauserHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ASplatt_HauserHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ASplatt_HauserHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASplatt_HauserHUD, 3847684268);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASplatt_HauserHUD(Z_Construct_UClass_ASplatt_HauserHUD, &ASplatt_HauserHUD::StaticClass, TEXT("/Script/Splatt_Hauser"), TEXT("ASplatt_HauserHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASplatt_HauserHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
