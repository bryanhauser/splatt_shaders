// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef SPLATT_HAUSER_Splatt_HauserProjectile_generated_h
#error "Splatt_HauserProjectile.generated.h already included, missing '#pragma once' in Splatt_HauserProjectile.h"
#endif
#define SPLATT_HAUSER_Splatt_HauserProjectile_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplatt_HauserProjectile(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserProjectile(); \
public: \
	DECLARE_CLASS(ASplatt_HauserProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ASplatt_HauserProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASplatt_HauserProjectile(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserProjectile(); \
public: \
	DECLARE_CLASS(ASplatt_HauserProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ASplatt_HauserProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplatt_HauserProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplatt_HauserProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatt_HauserProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatt_HauserProjectile(ASplatt_HauserProjectile&&); \
	NO_API ASplatt_HauserProjectile(const ASplatt_HauserProjectile&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatt_HauserProjectile(ASplatt_HauserProjectile&&); \
	NO_API ASplatt_HauserProjectile(const ASplatt_HauserProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatt_HauserProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplatt_HauserProjectile)


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ASplatt_HauserProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ASplatt_HauserProjectile, ProjectileMovement); }


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_9_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
