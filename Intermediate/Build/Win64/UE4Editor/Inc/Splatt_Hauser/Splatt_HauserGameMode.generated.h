// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATT_HAUSER_Splatt_HauserGameMode_generated_h
#error "Splatt_HauserGameMode.generated.h already included, missing '#pragma once' in Splatt_HauserGameMode.h"
#endif
#define SPLATT_HAUSER_Splatt_HauserGameMode_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_RPC_WRAPPERS
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplatt_HauserGameMode(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserGameMode(); \
public: \
	DECLARE_CLASS(ASplatt_HauserGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Splatt_Hauser"), SPLATT_HAUSER_API) \
	DECLARE_SERIALIZER(ASplatt_HauserGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASplatt_HauserGameMode(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserGameMode(); \
public: \
	DECLARE_CLASS(ASplatt_HauserGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Splatt_Hauser"), SPLATT_HAUSER_API) \
	DECLARE_SERIALIZER(ASplatt_HauserGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SPLATT_HAUSER_API ASplatt_HauserGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplatt_HauserGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SPLATT_HAUSER_API, ASplatt_HauserGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SPLATT_HAUSER_API ASplatt_HauserGameMode(ASplatt_HauserGameMode&&); \
	SPLATT_HAUSER_API ASplatt_HauserGameMode(const ASplatt_HauserGameMode&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SPLATT_HAUSER_API ASplatt_HauserGameMode(ASplatt_HauserGameMode&&); \
	SPLATT_HAUSER_API ASplatt_HauserGameMode(const ASplatt_HauserGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SPLATT_HAUSER_API, ASplatt_HauserGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplatt_HauserGameMode)


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_9_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
