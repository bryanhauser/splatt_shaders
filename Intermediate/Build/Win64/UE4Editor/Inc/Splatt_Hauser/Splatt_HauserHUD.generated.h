// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATT_HAUSER_Splatt_HauserHUD_generated_h
#error "Splatt_HauserHUD.generated.h already included, missing '#pragma once' in Splatt_HauserHUD.h"
#endif
#define SPLATT_HAUSER_Splatt_HauserHUD_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_RPC_WRAPPERS
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplatt_HauserHUD(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserHUD(); \
public: \
	DECLARE_CLASS(ASplatt_HauserHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ASplatt_HauserHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASplatt_HauserHUD(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ASplatt_HauserHUD(); \
public: \
	DECLARE_CLASS(ASplatt_HauserHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ASplatt_HauserHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplatt_HauserHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplatt_HauserHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatt_HauserHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatt_HauserHUD(ASplatt_HauserHUD&&); \
	NO_API ASplatt_HauserHUD(const ASplatt_HauserHUD&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatt_HauserHUD(ASplatt_HauserHUD&&); \
	NO_API ASplatt_HauserHUD(const ASplatt_HauserHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatt_HauserHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatt_HauserHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplatt_HauserHUD)


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_9_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_Splatt_HauserHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
