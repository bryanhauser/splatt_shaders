// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef SPLATT_HAUSER_CentralPillar_generated_h
#error "CentralPillar.generated.h already included, missing '#pragma once' in CentralPillar.h"
#endif
#define SPLATT_HAUSER_CentralPillar_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsPointBeingContested) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsPointBeingContested(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInControlTeam) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=this->InControlTeam(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetPointProgression) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->getPointProgression(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSphereEndOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnSphereEndOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSphereBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnSphereBeginOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsPointBeingContested) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsPointBeingContested(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execInControlTeam) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=this->InControlTeam(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execgetPointProgression) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->getPointProgression(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSphereEndOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnSphereEndOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSphereBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnSphereBeginOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACentralPillar(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ACentralPillar(); \
public: \
	DECLARE_CLASS(ACentralPillar, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ACentralPillar) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_INCLASS \
private: \
	static void StaticRegisterNativesACentralPillar(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ACentralPillar(); \
public: \
	DECLARE_CLASS(ACentralPillar, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ACentralPillar) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACentralPillar(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACentralPillar) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACentralPillar); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACentralPillar); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACentralPillar(ACentralPillar&&); \
	NO_API ACentralPillar(const ACentralPillar&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACentralPillar(ACentralPillar&&); \
	NO_API ACentralPillar(const ACentralPillar&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACentralPillar); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACentralPillar); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACentralPillar)


#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_PRIVATE_PROPERTY_OFFSET
#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_10_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_CentralPillar_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
