// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Splatt_HauserGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSplatt_HauserGameMode() {}
// Cross Module References
	SPLATT_HAUSER_API UClass* Z_Construct_UClass_ASplatt_HauserGameMode_NoRegister();
	SPLATT_HAUSER_API UClass* Z_Construct_UClass_ASplatt_HauserGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Splatt_Hauser();
// End Cross Module References
	void ASplatt_HauserGameMode::StaticRegisterNativesASplatt_HauserGameMode()
	{
	}
	UClass* Z_Construct_UClass_ASplatt_HauserGameMode_NoRegister()
	{
		return ASplatt_HauserGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_ASplatt_HauserGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_Splatt_Hauser,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "Splatt_HauserGameMode.h" },
				{ "ModuleRelativePath", "Splatt_HauserGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ASplatt_HauserGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ASplatt_HauserGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASplatt_HauserGameMode, 3416171452);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASplatt_HauserGameMode(Z_Construct_UClass_ASplatt_HauserGameMode, &ASplatt_HauserGameMode::StaticClass, TEXT("/Script/Splatt_Hauser"), TEXT("ASplatt_HauserGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASplatt_HauserGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
