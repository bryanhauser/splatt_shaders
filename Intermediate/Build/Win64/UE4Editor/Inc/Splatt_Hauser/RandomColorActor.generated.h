// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATT_HAUSER_RandomColorActor_generated_h
#error "RandomColorActor.generated.h already included, missing '#pragma once' in RandomColorActor.h"
#endif
#define SPLATT_HAUSER_RandomColorActor_generated_h

#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChangeGrey) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangeGrey(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeBlue) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangeBlue(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeRed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangeRed(); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChangeGrey) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangeGrey(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeBlue) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangeBlue(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execChangeRed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ChangeRed(); \
		P_NATIVE_END; \
	}


#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARandomColorActor(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ARandomColorActor(); \
public: \
	DECLARE_CLASS(ARandomColorActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ARandomColorActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesARandomColorActor(); \
	friend SPLATT_HAUSER_API class UClass* Z_Construct_UClass_ARandomColorActor(); \
public: \
	DECLARE_CLASS(ARandomColorActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Splatt_Hauser"), NO_API) \
	DECLARE_SERIALIZER(ARandomColorActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARandomColorActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARandomColorActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARandomColorActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARandomColorActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARandomColorActor(ARandomColorActor&&); \
	NO_API ARandomColorActor(const ARandomColorActor&); \
public:


#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARandomColorActor(ARandomColorActor&&); \
	NO_API ARandomColorActor(const ARandomColorActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARandomColorActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARandomColorActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARandomColorActor)


#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_PRIVATE_PROPERTY_OFFSET
#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_9_PROLOG
#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_RPC_WRAPPERS \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_INCLASS \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_INCLASS_NO_PURE_DECLS \
	Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Splatt_Hauser_Source_Splatt_Hauser_RandomColorActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
